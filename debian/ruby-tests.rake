require 'rake/testtask'

Rake::TestTask.new(:test) do |t|
  t.ruby_opts    = %w[ -w ]
  t.libs         = %w[ lib spec test ]
  t.pattern      = "{test,spec}/**/{test_*,*_spec}.rb"
end

task :default => :test
